+++
title = "CV"
description = "A swiss knife for the web"
date = "2019-02-28"
+++


### eCommerce infrasturcture specialist
<June 2020 - present>\
Sila

- Developing two Woocommerce based shops
- Creating custom PHP modules
- Administrating Linux servers with Docker and Docker-Compose stack


### Junior DevOps
<May 2019 - May 2020>\
RatioWeb

- Managing Continious Integration workflow using Gitlab and Kubernetes
- Administrating Linux servers
- Developing web apps 


### Co-founder / Board Member 
<Apr 2017 - Jul 2019>\
Browar Sabotaz

- Administrating Linux servers
- Managing and developing websites and web services
- Strategic planing, decision making

### Blog Author 
<Jan 2014 - Dec 2016>\
De Gruyter Open Access

- Creating content
- Managing blog and social media
- Analysing data and providing reports

### Web Developer
<Apr 2011 - Apr 2015>\
Freelancer 

- Developing great websites