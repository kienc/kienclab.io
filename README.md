This is a simple, static, retro CV website. 

It uses [Hugo](https://gohugo.io/) with [hugo.386](https://themes.gohugo.io/hugo.386/) theme, that I modifide to meet my needs. 

Content of the CV is in 'content' directory, and can be easly modified.

Docker compose file is attached to speed up a local set up. It is configured to allow you to run local version of the websire under a 'cv.local' domain instead of using bare port. It might be helpful if you sometimes work on multiple Hugo projects. If it is nor your case, you can ignore it.

To use a docerized local setup clone the repo and in the project's root folder run: 

<pre><code>
docker-compose up -d 
</code></pre>

Then change your local hosts file to match 'cv.local' address with localhost's IP. The website will be there after you build it with a standard command:

<pre><code>
hugo
</code></pre>


